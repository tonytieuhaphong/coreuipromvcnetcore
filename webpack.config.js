var path = require('path')
var webpack = require('webpack')
const glob = require('glob')
//const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const TerserPlugin = require('terser-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const RemoveStrictPlugin = require('remove-strict-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

// const HEAVY_LIBS = [
//     "ag-grid-community",
//     "ag-grid-enterprise",
//     'ag-grid-vue',
//     "flexmonster",
// ]

const entries = {
    //vendor: VENDOR_LIBS
}

const IGNORE_PATHS = [
    'unused'
]

glob.sync('./VueScripts/views/**/main.js').forEach(path => {
    const chunk = path.split('./VueScripts/views/')[1].split('/main.js')[0]
    if (IGNORE_PATHS.every(path => !chunk.includes(path))) {
        if (!chunk.includes('/')) {
            entries[chunk] = path
        } else {
            const joinChunk = chunk.split('/').join('-')
            entries[joinChunk] = path
        }
    }
});

entries['styles'] = path.join(__dirname, 'VueScripts/utils/styles.js');

const IS_PRODUCTION = (process.env.NODE_ENV === 'production');
if (IS_PRODUCTION) {
    console.log("Bundling in PRODUCTION mode")
} else {
    console.log("Bundling in DEVELOPMENT mode")
}


module.exports = {
    mode: IS_PRODUCTION ? 'production' : 'development',
    entry: entries,
    output: {
        path: path.resolve(__dirname, './wwwroot'),
        publicPath: '/',
        filename: 'js/[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(css|sass|scss)$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader',
                    'postcss-loader?parser=postcss-scss'
                ],
                exclude: path.join(__dirname, 'VueScripts/assets/scss/style.scss')
            },
            {
                test: path.join(__dirname, 'VueScripts/assets/scss/style.scss'),
                use: [
                    MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { url: true, sourceMap: true } },
                    { loader: 'sass-loader', options: { sourceMap: true } },
                    'postcss-loader?parser=postcss-scss'
                ]
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
                        // the "scss" and "sass" values for the lang attribute to the right configs here.
                        // other preprocessors should work out of the box, no loader config like this necessary.
                        'scss': [
                            'vue-style-loader',
                            'css-loader',
                            'sass-loader',
                            'postcss-loader?parser=postcss-scss'
                        ],
                        'sass': [
                            'vue-style-loader',
                            'css-loader',
                            'sass-loader?indentedSyntax',
                            'postcss-loader?parser=postcss-scss'
                        ],
                        postcss: [require('autoprefixer')()]
                    }
                    // other vue-loader options go here
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                options: {
                    plugins: [
                        "@babel/plugin-syntax-dynamic-import"
                    ]
                },
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpg|gif)$/,
                loader: 'file-loader',
                options: {
                    name: 'assets/images/[name].[ext]'
                }
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    'name': 'assets/fonts/[name].[ext]'
                }
            },
            {
                test: /\.(mov|mp4)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'assets/video/[name].[ext]'
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        modules: [
            'VueScripts',
            'node_modules'
        ],
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        },
        extensions: ['*', '.js', '.vue', '.json']
    },
    devServer: {
        historyApiFallback: true,
        noInfo: true,
        overlay: true
    },
    performance: {
        hints: false
    },
    plugins: [
        new VueLoaderPlugin(),
        new CompressionPlugin(),
        new MiniCssExtractPlugin({
            filename: 'css/[name].css'
        }),
        new OptimizeCssAssetsPlugin({
            cssProcessor: require('cssnano'),
            cssProcessorPluginOptions: {
                preset: ['default', { discardComments: { removeAll: true } }],
            },
            canPrint: true
        })
        /*new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.optimize\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorPluginOptions: {
            preset: ['default', { discardComments: { removeAll: true } }],
            },
            canPrint: true
        })*/
    ],
    //devtool: '#cheap-module-eval-source-map',
    devtool: 'none', // for ie
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            minSize: 0,
            cacheGroups: {
                //default: false,
                //vendors: false,
                polyfill: {
                    name: 'polyfill',
                    chunks: 'all',
                    test: /[\\/]node_modules[\\/](@babel|core-js)[\\/]/,
                    priority: 20,
                    enforce: true
                },
                core: {
                    name: 'core',
                    chunks: 'all',
                    test: /[\\/]node_modules[\\/](bootstrap-vue|vue|vuelidate|@coreui|css-loader|font-awesome|popper.js|portal-vue|process|regenerator-runtime|setimmediate|timers-browserify|vue-functional-data-merge|vue-loader|vue-style-loader|webpack)[\\/]/,
                    priority: 20,
                    enforce: true
                },
                // heavy chunk
                heavy: {
                    name: 'heavy',
                    // async + async chunks
                    chunks: 'all',
                    //chunks: (chunk) => { return HEAVY_LIBS.includes(chunk); }
                    // import file path containing node_modules
                    test: /[\\/]node_modules[\\/](ag-grid-community|ag-grid-enterprise|ag-grid-vue|flexmonster|@ckeditor)[\\/]/,
                    // test(module, chunks) {
                    //     const context = module.context;
                    //     // // const targets = ["vue-css-donut-chart"];
                    //     const targets = HEAVY_LIBS;
                    //     // if(context.indexOf(`bootstrap-vue`) > -1) {
                    //     //     console.log('separating bootstrap-vue');
                    //     // }
                    //     // return context.indexOf(`bootstrap-vue`) > -1;
                    //     // if(module.context.indexOf('ag-grid-community') > -1) {
                    //     //     console.log(module.context, );
                    //     // }
                    //     return context && context.indexOf("node_modules") >= 0 && !targets.find(t => context.indexOf(`/${t}`) > -1);
                    // },
                    //minSize: 0,
                    priority: 20, // common modules will go here
                    enforce: true
                },
                // vendor chunk
                vendor: {
                    // name of the chunk
                    name: 'vendor',
                    // async + async chunks
                    chunks: 'all',
                    // import file path containing node_modules
                    test: /[\\/]node_modules[\\/]/,
                    // test(module, chunks) {
                    //     const context = module.context;
                    //     const targets = ['bootstrap-vue'];
                    //     if(!targets.find(t => context.indexOf(`/${t}`) > -1)) {
                    //         console.log('not including bootstrap-vue');
                    //     }
                    //     return context && context.indexOf("node_modules") >= 0 && !targets.find(t => context.indexOf(`/${t}`) > -1);
                    //     // if(module.context.indexOf('ag-grid-community') > -1) {
                    //     //     console.log(module.context, );
                    //     // }
                    //     // return HEAVY_LIBS.some(x => module.context.indexOf(x) > -1);
                    // },
                    // priority
                    priority: 10,
                    enforce: true
                }
            }
        }
    }
}

if (IS_PRODUCTION) {

    //module.exports.devtool = '#source-map'
    module.exports.devtool = 'none'  // for ie
    // http://vue-loader.vuejs.org/en/workflow/production.html
    module.exports.plugins = (module.exports.plugins || []).concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true
        }),
        new RemoveStrictPlugin()
    ])
    module.exports.optimization.minimizer = [
        new TerserPlugin({
            cache: true,
            parallel: true,
        })
    ]
    // module.exports.optimization = {
    //     minimizer: [new TerserPlugin()],
    // //     minimizer: [
    // //         new TerserPlugin({
    // //             cache: true,
    // //             parallel: true,
    // //             sourceMap: true, // Must be set to true if using source-maps in production
    // //             terserOptions: {
    // //                 warnings: false,
    // //                 ie8: false
    // //             }
    // //         }),
    // //     ],
    // //     noEmitOnErrors: true,
    // //     moduleIds: 'hashed',
    // //     chunkIds: 'total-size'
    // }
}

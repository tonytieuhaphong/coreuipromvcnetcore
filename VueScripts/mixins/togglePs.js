"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.togglePs = void 0;
var togglePs = {
  methods: {
    togglePs: function togglePs(toggle) {
      var sidebar = document.querySelector('.sidebar-nav section');

      if (sidebar) {
        // sidebar.classList.toggle('ps', toggle);
        // sidebar.classList.toggle('ps-container', toggle);
        // sidebar.classList.toggle('ps--active-y', toggle);
        if (toggle) {
          sidebar.classList.add('ps', 'ps-container', 'ps--active-y');
        } else {
          sidebar.classList.remove('ps', 'ps-container', 'ps--active-y');
        }
      }
    }
  }
};
exports.togglePs = togglePs;

export default {
  items: [
    {
      name: 'Dashboard',
      url: '/Home/Index',
      icon: 'fal fa-home',
      badge: {
        variant: 'primary',
        text: 'NEW'
      }
    },
    {
      name: 'Privacy',
      url: '/Home/Privacy',
      icon: 'icon-speedometer',
    }
  ]
}

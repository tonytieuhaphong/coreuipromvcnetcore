﻿import Vue from 'vue'
import appInit from 'utils/appInit'

import App from './App'

new Vue({
    el: '#app',
    template: '<App/>',
    components: {
        App
    }
})

﻿import Vue from 'vue'
import appInit from 'utils/appInit'

import App from './App'

// import Vuelidate from 'vuelidate'
// Vue.use(Vuelidate);

new Vue({
    el: '#app',
    template: '<App/>',
    components: {
        App
    }
})

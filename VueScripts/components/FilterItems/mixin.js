let mixin = {
    props: {
        keyword: String,
        label: String,
        description: String,
        initialValue: String,
        parse: {
            type: Function,
            default: function(value) { return value }
        },
        format: {
            type: Function,
            default: function(value) { return value }
        }
    },
    data() {
        return {
            value: ''  
        }
    },
    computed: {
        formattedValue() {
            return this.format(this.value)
        }
    },
    methods: {
        parseInitialValue() {
            const initialValue = this.parse(this.initialValue); 
            this.value = initialValue;
        }
    },
    mounted() {
        this.parseInitialValue()  
    },
    watch: {
        initialValue(newValue, value) {
            this.parseInitialValue();
        }
    }
}

export default mixin;
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import '../polyfill' // ie 11

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);

import axiosInit from './axiosInit';
axiosInit();


Vue.component('layout-container', () => import(/* webpackChunkName: "layout-container" */ 'containers/layout/DefaultContainer'));
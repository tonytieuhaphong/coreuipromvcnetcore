import axios from 'axios';
import Vue from 'vue';

const onResponseSuccess = (config) => {
  return config;
};

const onResponseError = (error) => {
  if (!!error && !!error.response) {
    if (error.response.status === 401) {
      Vue.prototype.$axiosEventHub.$emit('unauthorize-error');
    }
    else if (error.response.status === 403) {
      Vue.prototype.$axiosEventHub.$emit('forbidden-error');
    }
    else if (error.response.status === 409) {
      Vue.prototype.$axiosEventHub.$emit('concurrent-login-error');
    }
    else if (error.response.status === 503) {
      Vue.prototype.$axiosEventHub.$emit('service-unavailable');
    }
  }
  return Promise.reject(error);
}

export default function axiosInit() {
  if (!Vue.prototype.$axiosEventHub) {
    Vue.prototype.$axiosEventHub = new Vue();
  }

  axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
  axios.interceptors.response.use(onResponseSuccess, onResponseError);
}